package com.project.tetris.windows;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TetrisWindow extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	private TetrisGameBoard gameBoard;
	private TetrisInformationBoard informationBoard;
	private JPanel container = new JPanel();
	private GameInformation game;
	private boolean inGame = false;
	private boolean incraseSpeed = false;
	private boolean cheatMode = false;
	private Thread myGameThread = null;
	private long timeSinceLastFrame = 0;
	private long previousTime = 0;

	private JMenuBar menuBar = new JMenuBar();
	
	private JMenu fileMenu = new JMenu("File"),
				editMenu = new JMenu("edit"),
				gameMenu = new JMenu("game"),
				aboutMenu = new JMenu("about");
				
	

	public TetrisWindow()
	{		
		int  squareSize = 30, gameBoardHeight = 20, gameBoardWidth = 10;

		game = new GameInformation(gameBoardWidth, gameBoardHeight, squareSize);
		
		gameBoard = new TetrisGameBoard(game);
		informationBoard =  new TetrisInformationBoard(game);
		
        setTitle("Tetris");
        setSize((gameBoardWidth + 3) * squareSize + 32, (gameBoardHeight) * squareSize + 60);
        setLocationRelativeTo(null);   
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);  

        container.add(gameBoard);
        container.add(informationBoard);
        container.setBackground(new Color(225, 225, 225));
        
        setContentPane(container); 
        setVisible(true);
        
        this.addKeyListener(new KeyboardListener());
        initMenu();
    
    }
	
	private void initMenu(){
		
		
		
		fileMenu.setMnemonic('F');
		menuBar.add(fileMenu);
		
		editMenu.setMnemonic('E');
		menuBar.add(editMenu);
		
		gameMenu.setMnemonic('G');
		menuBar.add(gameMenu);
		
		aboutMenu.setMnemonic('A');
		menuBar.add(aboutMenu);
		
		setJMenuBar(menuBar);
	}
	
	 public void startRendering()
	 {
		 previousTime = System.currentTimeMillis();
		 
		 do
         {			 
			 int breakDuration = (int) (game.getBreakFactorA() * game.getScore() + game.getBreakFactorB());
			 if(breakDuration < game.getMinimumBreakDuration())
				 breakDuration = game.getMinimumBreakDuration();
			 
			 timeSinceLastFrame = (System.currentTimeMillis() - previousTime);

			 if(!incraseSpeed)
			 {
				 if(timeSinceLastFrame < breakDuration)
				 {
					 try {
		                 Thread.sleep(breakDuration - timeSinceLastFrame);
			         } catch (InterruptedException e) {
			                 e.printStackTrace();
			         }
				 }
			 }
			 else
			 {
				 if(timeSinceLastFrame < 42)
				 {
					 try {
		                 Thread.sleep(42 - timeSinceLastFrame);
			         } catch (InterruptedException e) {
			                 e.printStackTrace();
			         }
				 }
			 }
			 
			 previousTime = System.currentTimeMillis();
			 
			 if(!renderOneFrame())
			 {
				 JOptionPane.showMessageDialog(null, "You lose! Your score: " + game.getScore(), "Game Over", JOptionPane.INFORMATION_MESSAGE);
				 inGame = false;
				 myGameThread = null;
				 incraseSpeed = false;
				 game.init();
			 }			 
			 
         }while(inGame);         
	 }
	 
	 public boolean renderOneFrame()
	 {
		 boolean response = gameBoard.frameStarted();
		 		
         gameBoard.repaint();
         //<tmp>
         informationBoard.repaintScore();
         informationBoard.repaintNextTetriminos();
         //</tmp>
         return response;
	 }
	 
	 class GameThread implements Runnable
	 {		 
         @Override
         public void run() {
        	 inGame = true;
        	 startRendering();
         }
	 }
	 
	 class KeyboardListener implements KeyListener
	 {		 
		 private long lastPressProcessedPressed = 0;
		 private long lastPressProcessedReleased = 0;
	    	
		 public void keyPressed(KeyEvent event) {
			 if(System.currentTimeMillis() - lastPressProcessedPressed > 50)
			 {
				 lastPressProcessedPressed = System.currentTimeMillis();
				 
				 switch(event.getKeyCode())
				 {
				 	case KeyEvent.VK_SHIFT:
				 		incraseSpeed = true;
				 		break;
				 	case KeyEvent.VK_UP :
				 	case KeyEvent.VK_Z :
				 		if(inGame || cheatMode)
				 		{
					 		game.getActualTetriminos().rotateLeft();
					 		gameBoard.repaint();
				 		}
				 		break;
				 	case KeyEvent.VK_DOWN :
				 	case KeyEvent.VK_S :
				 		if(inGame || cheatMode)
				 		{
				 			game.getActualTetriminos().rotateRight();
				 			gameBoard.repaint();
				 		}
				 		break;
				 	case KeyEvent.VK_LEFT:
				 	case KeyEvent.VK_Q :
				 		if(inGame || cheatMode)
				 		{
				 			game.getActualTetriminos().moveLeft();
				 			gameBoard.repaint();
				 		}
				 		break;
				 	case KeyEvent.VK_RIGHT :
				 	case KeyEvent.VK_D :
				 		if(inGame || cheatMode)
				 		{
				 			game.getActualTetriminos().moveRight();
				 			gameBoard.repaint();
				 		}
				 		break;
				 	}
			 	}				
			}

			public void keyReleased(KeyEvent event) {				

				 if(System.currentTimeMillis() - lastPressProcessedReleased > 50) {
					 lastPressProcessedReleased = System.currentTimeMillis();					 
					 
					 switch(event.getKeyCode())
					 {
					 	case KeyEvent.VK_P :
					 		if(myGameThread == null){
								myGameThread = new Thread(new GameThread());
						        myGameThread.start();
							 }
							 else
							 {
								 myGameThread = null;
								 inGame = false;
							 }
					 		break;
					 	case KeyEvent.VK_SHIFT:
					 		incraseSpeed = false;
					 		break;
					 	case KeyEvent.VK_PAGE_UP :
					 		if(cheatMode)
					 		{
					 			game.changeNextTetriminos(-1);
					 			informationBoard.repaintNextTetriminos();
					 		}
					 		break;
					 	case KeyEvent.VK_PAGE_DOWN :
					 		if(cheatMode)
					 		{
					 			game.changeNextTetriminos(1);
					 			informationBoard.repaintNextTetriminos();
					 		}
					 		break;
					 	case KeyEvent.VK_M :
					 		game.setDisplayMode(game.getDisplayMode() + 1);
					 		informationBoard.repaintNextTetriminos();
					 		gameBoard.repaint();
					 		break;
					 }					 
			     }							
			}

			public void keyTyped(KeyEvent event)
			{
			}
	 }
}
