package com.project.tetris.windows;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
 
public class NextTetriminosInformationBoard extends JPanel
{ 
	private static final long serialVersionUID = 1L;

    GameInformation game;
    
    public NextTetriminosInformationBoard(GameInformation g)
    {
    	game = g;
    }

	public void paintComponent(Graphics g)
	{				
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(new Color(225, 225, 225));
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		GradientPaint gp;
		
		if(game.getDisplayMode() < 4)
			gp = new GradientPaint(8, this.getHeight() - 260, new Color(255, 255, 255), game.getSquareSize() * 3, this.getHeight() - 80, new Color(200, 200, 200), true);
		else
			gp = new GradientPaint(8, this.getHeight() - 260, new Color(200, 200, 200), game.getSquareSize() * 3, this.getHeight() - 80, new Color(145, 145, 145), true);
		
		g2d.setPaint(gp);
		g2d.fillRect(8, this.getHeight() - (game.getSquareSize() * 7 + 10), game.getSquareSize() * 3, game.getSquareSize() * 6);
		
		game.getNextTetriminos().paint(g2d,  this, 8, this.getHeight() - (game.getSquareSize() * 7 + 10), game.getSquareSize() * 3, game.getSquareSize() * 6);
	}   
}