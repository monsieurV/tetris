package com.project.tetris.windows;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.util.Random;
import com.project.tetris.tetriminos.*;

public class GameInformation
{	
	private int score;
	private int gameBoardWidth;
	private int gameBoardHeight;
	private int squareSize;
	private int[][] gameBoard;
	private Tetriminos actualTetriminos;
	private Tetriminos nextTetriminos;
	private int displayMode = 0;
	private float breakFactorA = -0.5f;
	private int breakFactorB = 600;
	private int minimumBreakDuration = 42;
	
	
	public GameInformation(int gbWidth, int gbHeight, int sqSize)
	{
		gameBoardWidth = gbWidth;
		gameBoardHeight = gbHeight;		
		gameBoard = new int[gameBoardWidth][gameBoardHeight];
		squareSize = sqSize;
		
		init();
	}
	
	public void init()
	{
		for(int x  = 0; x < gameBoardWidth; x++)
		{
			for(int y = 0; y < gameBoardHeight; y++)
			{
				gameBoard[x][y] = 0;
			}
		}
		
		score = 0;
		actualTetriminos = generateRandomTetriminos();
		nextTetriminos = generateRandomTetriminos();		
	}
	
	Tetriminos generateRandomTetriminos()
	{
		int randomNumber;
		
		Random random = new Random();
		randomNumber = random.nextInt(7);

		switch (randomNumber) {
			case 0 : 
				return new Tetriminos1(this);
			case 1 :
				return new Tetriminos2(this);
			case 2 :
				return new Tetriminos3(this);
			case 3 :
				return new Tetriminos4(this);
			case 4 :
				return new Tetriminos5(this);
			case 5 :
				return new Tetriminos6(this);
			default :
				return new Tetriminos7(this);
		}
	}
	
	void changeNextTetriminos(int changeValue)
	{
		if(changeValue == 0)
		{
			nextTetriminos =  generateRandomTetriminos();
		}
		else
		{
			int newTetriminosId = (int) (nextTetriminos.getId() + changeValue - 1) % 7;
			
			if(newTetriminosId < 0)
			{
				newTetriminosId += 7;
			}
			
			switch (newTetriminosId) {
				case 0 : 
					nextTetriminos =  new Tetriminos1(this);
					break;
				case 1 :
					nextTetriminos =  new Tetriminos2(this);
					break;
				case 2 :
					nextTetriminos =  new Tetriminos3(this);
					break;
				case 3 :
					nextTetriminos =  new Tetriminos4(this);
					break;
				case 4 :
					nextTetriminos =  new Tetriminos5(this);
					break;
				case 5 :
					nextTetriminos =  new Tetriminos6(this);
					break;
				case 6 :
					nextTetriminos =  new Tetriminos7(this);
					break;
			}
		}		
	}
	
	
	
	public void updateTetriminos() {
		
		for(int x = 0; x < actualTetriminos.getWidth(); x++)
		{
			for(int y = 0; y < actualTetriminos.getHeight(); y++)
			{
				if(actualTetriminos.getMatrice().get(x).get(y) == 1)
				{
					if(x + actualTetriminos.getPosX() >= 0 && y + actualTetriminos.getPosY() >= 0)
    				{
						gameBoard[x + actualTetriminos.getPosX()][y + actualTetriminos.getPosY()] = actualTetriminos.getId();
    				}
				}
			}
		}
		
		int numberOfLine = 0;
		
		for(int y  = gameBoardHeight - 1; y > -1; y --)
		{
			boolean fullLine = true;
			
			for(int x = 0; x < gameBoardWidth; x++)
			{
				if(gameBoard[x][y] == 0)
				{
					fullLine = false;
					break;
				}
			}
			
			if(fullLine)
			{
				numberOfLine++;
				
				for(int j = y; j > 0; j--)
				{
					for(int i = 0; i < gameBoardWidth; i++)
					{
						gameBoard[i][j] = gameBoard[i][j-1];
					}
				}
				y++;
			}
		}
		
		switch(numberOfLine)
		{
			case 1:
				score+=10;
				break;
			case 2:
				score+=30;
				break;
			case 3:
				score+=50;
				break;
			case 4:
				score+=80;
				break;
		}

		actualTetriminos = nextTetriminos;
		nextTetriminos = generateRandomTetriminos();
	}
	
	public void paintBoard(Graphics2D g2d)
	{
		GradientPaint gp;
		
		for(int x = 0; x < gameBoardWidth; x++)
		{
			for(int y = 0; y < gameBoardHeight; y++)
			{
				switch(displayMode)
				{
					case 0:
				}
				if(gameBoard[x][y] != 0 && displayMode < 3 || gameBoard[x][y] == 0 && displayMode >= 3)
				{
					Color[] tetriminosColor = new Color[4];
					
					switch(displayMode)
					{
						case 1:
						case 2:
							tetriminosColor[0] = new Color(128, 128, 128);
							tetriminosColor[1] = new Color(0, 0, 0);
							tetriminosColor[2] = new Color(255, 255, 255);
							tetriminosColor[3] = new Color(128, 128, 128);
							break;
						default:
							switch(gameBoard[x][y])
							{
								case 1:
									tetriminosColor[0] = Tetriminos1.tetriminosColor0;
									tetriminosColor[1] = Tetriminos1.tetriminosColor1;
									tetriminosColor[2] = Tetriminos1.tetriminosColor2;
									tetriminosColor[3] = Tetriminos1.tetriminosColor3;
									break;
								case 2:
									tetriminosColor[0] = Tetriminos2.tetriminosColor0;
									tetriminosColor[1] = Tetriminos2.tetriminosColor1;
									tetriminosColor[2] = Tetriminos2.tetriminosColor2;
									tetriminosColor[3] = Tetriminos2.tetriminosColor3;
									break;
								case 3:
									tetriminosColor[0] = Tetriminos3.tetriminosColor0;
									tetriminosColor[1] = Tetriminos3.tetriminosColor1;
									tetriminosColor[2] = Tetriminos3.tetriminosColor2;
									tetriminosColor[3] = Tetriminos3.tetriminosColor3;
									break;
								case 4:
									tetriminosColor[0] = Tetriminos4.tetriminosColor0;
									tetriminosColor[1] = Tetriminos4.tetriminosColor1;
									tetriminosColor[2] = Tetriminos4.tetriminosColor2;
									tetriminosColor[3] = Tetriminos4.tetriminosColor3;
									break;
								case 5:
									tetriminosColor[0] = Tetriminos5.tetriminosColor0;
									tetriminosColor[1] = Tetriminos5.tetriminosColor1;
									tetriminosColor[2] = Tetriminos5.tetriminosColor2;
									tetriminosColor[3] = Tetriminos5.tetriminosColor3;
									break;
								case 6:
									tetriminosColor[0] = Tetriminos6.tetriminosColor0;
									tetriminosColor[1] = Tetriminos6.tetriminosColor1;
									tetriminosColor[2] = Tetriminos6.tetriminosColor2;
									tetriminosColor[3] = Tetriminos6.tetriminosColor3;
									break;
								case 7:
									tetriminosColor[0] = Tetriminos7.tetriminosColor0;
									tetriminosColor[1] = Tetriminos7.tetriminosColor1;
									tetriminosColor[2] = Tetriminos7.tetriminosColor2;
									tetriminosColor[3] = Tetriminos7.tetriminosColor3;
									break;
								default:
									tetriminosColor[0] = new Color(128, 128, 128);
									tetriminosColor[1] = new Color(0, 0, 0);
									tetriminosColor[2] = new Color(255, 255, 255);
									tetriminosColor[3] = new Color(128, 128, 128);
							}
					}

					gp = new GradientPaint(0, y*squareSize, tetriminosColor[0], 0, (y + 1)*squareSize, tetriminosColor[1], false);
					g2d.setPaint(gp);
					g2d.fillRect(x*squareSize + 1, y*squareSize + 1, squareSize - 1, squareSize - 1);
					
					gp = new GradientPaint(0, y*squareSize, tetriminosColor[2], 0, (y + 1)*squareSize, tetriminosColor[3], false);
					g2d.setPaint(gp);
					g2d.fillRect(x*squareSize, y*squareSize, squareSize - 1, squareSize - 1);
				}
			}
		}
	}
	
	public boolean lose()
	{		
		for(int x = 0; x < gameBoardWidth; x++)
		{
			if(gameBoard[x][0] != 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public int[][] getGameBoard() {
		return gameBoard;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Tetriminos getActualTetriminos() {
		return actualTetriminos;
	}
	public Tetriminos getNextTetriminos() {
		return nextTetriminos;
	}
	public int getGameBoardWidth() {
		return gameBoardWidth;
	}
	public int getGameBoardHeight() {
		return gameBoardHeight;
	}	

	public int getSquareSize() {
		return squareSize;
	}

	public void setSquareSize(int squareSize) {
		this.squareSize = squareSize;
	}

	public int getDisplayMode() {
		return displayMode;
	}
	public void setDisplayMode(int displayMode) {
		
		if(displayMode < 0 || displayMode > 4 && displayMode < 42)
		{
			displayMode = 42;
		}
		else if(displayMode > 42)
		{
			displayMode = 0;
		}
		this.displayMode = displayMode;
	}

	public float getBreakFactorA() {
		return breakFactorA;
	}

	public void setBreakFactorA(float breakFactorA) {
		this.breakFactorA = breakFactorA;
	}

	public int getBreakFactorB() {
		return breakFactorB;
	}

	public void setBreakFactorB(int breakFactorB) {
		this.breakFactorB = breakFactorB;
	}

	public int getMinimumBreakDuration() {
		return minimumBreakDuration;
	}

	public void setMinimumBreakDuration(int minimumBreakDuration) {
		this.minimumBreakDuration = minimumBreakDuration;
	}	
}
