package com.project.tetris.windows;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;


import javax.swing.JLabel;
import javax.swing.JPanel;

public class TetrisInformationBoard  extends JPanel
{
	private static final long serialVersionUID = 1L;
	private JPanel container = new JPanel();
	private JLabel scoreLabel = new JLabel();
	NextTetriminosInformationBoard nextTetriminosBoard;
	private GameInformation game;
	
	public TetrisInformationBoard(GameInformation g)
    {
		game = g;
		nextTetriminosBoard = new NextTetriminosInformationBoard(game);
		
    	setPreferredSize(new Dimension(3 * game.getSquareSize() + 10, game.getGameBoardHeight() * game.getSquareSize()));
    	
    	container.setLayout(new BorderLayout());
    	container.add(scoreLabel, BorderLayout.NORTH);
    	container.add(nextTetriminosBoard, BorderLayout.CENTER);
    	
    	scoreLabel.setFont(new Font(scoreLabel.getFont().getName(), scoreLabel.getFont().getStyle(), (int) (game.getSquareSize() * 0.83)));
    	scoreLabel.setText("" + game.getScore());
    	
    	this.setLayout(new BorderLayout());
    	this.add(container, BorderLayout.CENTER);
    }
	
	public void repaintScore()
	{				
		scoreLabel.setText("" + game.getScore());
	}
	
	public void repaintNextTetriminos()
	{				
		nextTetriminosBoard.repaint();
	}	
}
