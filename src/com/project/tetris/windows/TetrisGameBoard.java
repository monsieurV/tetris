package com.project.tetris.windows;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
 
public class TetrisGameBoard extends JPanel
{ 
	private static final long serialVersionUID = 1L;

    private GameInformation game;
    
    public TetrisGameBoard(GameInformation g)
    {    	
    	game = g;    	
    	setPreferredSize(new Dimension(game.getGameBoardWidth() * game.getSquareSize(), game.getGameBoardHeight() * game.getSquareSize()));
    }
    
	public void paintComponent(Graphics g)
	{				
		Graphics2D g2d = (Graphics2D) g;
		
		if(game.getDisplayMode() == 42)
		{
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
			
			try
			{
				Image img = ImageIO.read(new File("img/trollFace.png"));
				g2d.drawImage(img, (game.getGameBoardWidth()*game.getSquareSize() - game.getGameBoardWidth() * game.getSquareSize()) / 2,
									(game.getGameBoardHeight()*game.getSquareSize() - game.getGameBoardHeight() * game.getSquareSize()) / 2, this);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{	GradientPaint gp;
			gp = new GradientPaint(0, 0, new Color(255, 255, 255), game.getGameBoardWidth() * game.getSquareSize(), game.getGameBoardHeight() * game.getSquareSize(), new Color(200, 200, 200), true);
			
			g2d.setPaint(gp);
			g2d.fillRect(0, 0, game.getGameBoardWidth() * game.getSquareSize(), game.getGameBoardHeight() * game.getSquareSize());
		}
		
		game.paintBoard(g2d);

		game.getActualTetriminos().paint(g2d);		
	}   
	
	public boolean frameStarted()
	{
		if(! game.getActualTetriminos().moveDown())
		{
			game.updateTetriminos();
		}
		
		return !game.lose();
	}
	
}