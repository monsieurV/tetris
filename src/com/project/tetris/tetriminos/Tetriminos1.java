package com.project.tetris.tetriminos;

import java.awt.Color;
import java.util.Vector;

import com.project.tetris.windows.GameInformation;

public class Tetriminos1 extends Tetriminos {
	
	public static final Color tetriminosColor0 = new Color(128, 128, 128);
	public static final Color tetriminosColor1 = new Color(128, 0, 0);
	public static final Color tetriminosColor2 = new Color(255, 128, 128);
	public static final Color tetriminosColor3 = new Color(255, 0, 0);
	
	public Tetriminos1(GameInformation g) {

		super(1, 1, 4, g);
		
		tetriminosColor[0] = tetriminosColor0;
		tetriminosColor[1] = tetriminosColor1;
		tetriminosColor[2] = tetriminosColor2;
		tetriminosColor[3] = tetriminosColor3;
		
		tetriminosMatrice = new Vector<Vector<Integer>>(0);		
		
		tetriminosMatrice.add(new Vector<Integer>(0));
			tetriminosMatrice.get(0).add(1);
			tetriminosMatrice.get(0).add(1);
			tetriminosMatrice.get(0).add(1);
			tetriminosMatrice.get(0).add(1);			
	}	
}
