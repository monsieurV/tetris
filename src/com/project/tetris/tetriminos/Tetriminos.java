package com.project.tetris.tetriminos;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.util.Vector;

import javax.swing.JPanel;

import com.project.tetris.windows.GameInformation;

public abstract class Tetriminos {
	
	protected int posX;
    protected int posY;
    protected GameInformation game;    
    protected Color[] tetriminosColor = new Color[4];
    protected Vector<Vector<Integer>> tetriminosMatrice;
    protected int id;
    
    public static final Color blackAndWhiteColor0 = new Color(128, 128, 128);
	public static final Color blackAndWhiteColor1 = new Color(0, 0, 0);
	public static final Color blackAndWhiteColor2 = new Color(255, 255, 255);
	public static final Color blackAndWhiteColor3 = new Color(128, 128, 128);
    
    public Tetriminos(int identifier, int width, int height, GameInformation g)
    {	
    	id = identifier;
    	game = g;
		posX = (game.getGameBoardWidth() - width) / 2;
		posY = -1 - height;
    }
    
    public void paint(Graphics2D g2d)
    {
    	paint(g2d, posX * game.getSquareSize(), posY * game.getSquareSize());
    }
    
    public void paint(Graphics2D g2d, JPanel parentPanel, int x, int y, int width, int height)
    {
		x += (width - getWidth() * game.getSquareSize()) / 2;
		y += (height - getHeight() * game.getSquareSize()) / 2;

		paint(g2d, x, y);
    }
    
    protected void paint(Graphics2D g2d, int  x, int y)
    {
		GradientPaint gp;
		
		for(int i = 0; i < getWidth(); i++)
		{
			for(int j = 0; j < getHeight(); j++)
			{
				if(tetriminosMatrice.get(i).get(j) == 1)
				{
					if(game.getDisplayMode() < 4)
					{
						Color[] tColor = new Color[4];
						
						switch(game.getDisplayMode())
						{
							case 1:
								tColor[0] = blackAndWhiteColor0;
								tColor[1] = blackAndWhiteColor1;
								tColor[2] = blackAndWhiteColor2;
								tColor[3] = blackAndWhiteColor3;
								break;
							default:
								tColor[0] = tetriminosColor[0];
								tColor[1] = tetriminosColor[1];
								tColor[2] = tetriminosColor[2];
								tColor[3] = tetriminosColor[3];
						}
						
						gp = new GradientPaint(0, j*game.getSquareSize() + y, tColor[0], 0, (j + 1)*game.getSquareSize() + y, tColor[1], false);
						g2d.setPaint(gp);
						g2d.fillRect(i*game.getSquareSize() + x + 1, j*game.getSquareSize() + y + 1, game.getSquareSize() - 1, game.getSquareSize() - 1);
						
						gp = new GradientPaint(0, j*game.getSquareSize() + y, tColor[2], 0, (j + 1)*game.getSquareSize() + y, tColor[3], false);
						g2d.setPaint(gp);
						g2d.fillRect(i*game.getSquareSize() + x,j*game.getSquareSize() + y, game.getSquareSize() - 1, game.getSquareSize() - 1);
					}
					else
					{
						gp = new GradientPaint(0, 0, new Color(255, 255, 255), game.getSquareSize() * game.getGameBoardWidth(), game.getSquareSize() * game.getGameBoardHeight(), new Color(200, 200, 200), true);
						g2d.setPaint(gp);
						g2d.fillRect(i*game.getSquareSize() + x, j*game.getSquareSize() + y, game.getSquareSize(), game.getSquareSize());
					}
				}
			}
		}
    }

	public boolean rotateLeft()
    {
    	Vector<Vector<Integer>> newTetriminosMatrice = new Vector<Vector<Integer>>(0);
    	
    	for(int j = 0; j < getHeight(); j++)
    	{
    		newTetriminosMatrice.add(new Vector<Integer>(0));
    		
    		for(int i = getWidth() - 1; i > -1; i--)
    		{
    			newTetriminosMatrice.get(j).add(tetriminosMatrice.get(i).get(j));
    		}
    	}
    	
    	int newPosX, newPosY;
    	
    	newPosX = calculateNewPos(posX, getWidth(), newTetriminosMatrice.size());
    	newPosY = calculateNewPos(posY, getHeight(), newTetriminosMatrice.get(0).size());
    	
    	if(canRotate(newTetriminosMatrice, newPosX, newPosY))
    	{
    		tetriminosMatrice = newTetriminosMatrice;
    		posX = newPosX;
    		posY = newPosY;
    		return true;
    	}
    	return false;    	
    }
    
    public boolean rotateRight()
    {
    	Vector<Vector<Integer>> newTetriminosMatrice = new Vector<Vector<Integer>>(0);
    	
    	for(int j = getHeight() - 1; j > -1; j--)
    	{
    		newTetriminosMatrice.add(new Vector<Integer>(0));
    		
    		for(int i = 0; i < getWidth(); i++)
    		{
    			newTetriminosMatrice.get(getHeight() - j - 1).add(tetriminosMatrice.get(i).get(j));
    		}
    	}
    	
    	int newPosX, newPosY;
    	
    	newPosX = calculateNewPos(posX, getWidth(), newTetriminosMatrice.size());
    	newPosY = calculateNewPos(posY, getHeight(), newTetriminosMatrice.get(0).size());
    	
    	if(canRotate(newTetriminosMatrice, newPosX, newPosY))
    	{
    		tetriminosMatrice = newTetriminosMatrice;
    		posX = newPosX;
    		posY = newPosY;
    		return true;
    	} 
    	return false;
    }
    
    public boolean moveLeft()
    {
    	return move(posX - 1, posY);
    }
    
    public boolean moveRight()
    {
    	return move(posX + 1, posY);
    }
    
    public boolean moveDown()
    {
    	return move(posX, posY + 1);
    }
    
    public boolean move(int newPosX, int newPosY)
    {
    	if(game.getActualTetriminos().canMove(newPosX, newPosY))
		{
			posX = newPosX;
			posY = newPosY;
			return true;
		}
    	return false;
    }
    
    int calculateNewPos(int actualPos, int acualSize, int newSize)
    {
    	int Center = (int) (2 * actualPos + acualSize) / 2;
    	return Center - (newSize / 2);
    }
    
    public boolean canRotate(Vector<Vector<Integer>> newTetriminosMatrice, int newPosX, int newPosY)
    {
    	if( newPosY >= game.getGameBoardHeight() - newTetriminosMatrice.get(0).size() ||
        	newPosX > game.getGameBoardWidth() - newTetriminosMatrice.size() ||
        	newPosX < 0)
    	{
    		return false;
    	}
    	
    	for(int x = 0; x < newTetriminosMatrice.size(); x++)
    	{
    		for(int y = 0; y < newTetriminosMatrice.get(0).size(); y++)
    		{
    			if(newTetriminosMatrice.get(x).get(y) == 1)
    			{
    				if(x + newPosX >= 0 && y + newPosY >= 0)
    				{
    					if(game.getGameBoard()[x + newPosX][y + newPosY] != 0)
    					{
    						return false;
    					}
    				}    				
    			}
    		}
    	}    	
    	return true;
    }
    
    public boolean canMove(int newPosX, int newPosY)
    {    	
    	if( newPosY > game.getGameBoardHeight() - getHeight() ||
    		newPosX > game.getGameBoardWidth() - getWidth() ||
    		newPosX < 0)
    	{
    		return false;
    	}
    	
    	for(int x = 0; x < getWidth(); x++)
    	{
    		for(int y = 0; y < getHeight(); y++)
    		{
    			if(tetriminosMatrice.get(x).get(y) == 1)
    			{
    				if(x + newPosX >= 0 && y + newPosY >= 0)
    				{
    					if(game.getGameBoard()[x + newPosX][y + newPosY] != 0)
    					{
    						return false;
    					}
    				}    				
    			}
    		}
    	}
    	
    	return true;
    }
    public int getWidth() {
		return tetriminosMatrice.size();
	}
    public int getHeight() {
		return tetriminosMatrice.get(0).size();
	}
	
	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
    
    public Vector<Vector<Integer>> getMatrice() {
		return tetriminosMatrice;
	}

	public int getId() {
		return id;
	}
    
}
